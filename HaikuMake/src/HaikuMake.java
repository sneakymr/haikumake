/* Thank you for taking the time to look at my code! I am excited to submit this as part
 * of my application to Hacker School. I created this program to parse all the entries in 
 * the Carnegie Mellon Pronouncing Dictionary, available for download here: 
 * 
 * http://www.speech.cs.cmu.edu/cgi-bin/cmudict
 * 
 * As it reads through the entries, the program determines how many syllables are in each
 * word and assigns them to an array that contains strings of words with the same number
 * of syllables. 
 * 
 * Finally, it randomly selects words from the appropriate arrays to create and print a haiku 
 * with a 5-7-5 syllable format. Although the program is "computationally expensive", I have 
 * learned many useful Java methods and debugging techniques while making it. My future goal 
 * is to create a GUI interface that allows the user to click a button and automatically 
 * generate a new haiku, without running through the entire process of reading the CMU 
 * Dictionary each time.
 * 
 * I also see this as a fun beginning to learning more about text-parsing techniques and 
 * creating more fun programs that randomly create a variety of poetry formats on command.
 * 
 * Cheers,
 * Andrew Sparks
 * 
 * This code is also stored on BitBucket: https://sneakymr@bitbucket.org/sneakymr/haikumake.git
 */

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Scanner;

public class HaikuMake {
	
	public static void main(String args[]) throws IOException {
		
		/* Creates a new file scanner to read in contents of the Carnegie Mellon Dictionary,
		 * located in the project folder with this program.
		 */
		Scanner dictScanner = new Scanner(new File("cmudict.txt"));
		
		ArrayList<String>  
		array2 = new ArrayList<String>(), //ArrayList of one syllable words
		array3 = new ArrayList<String>(), //ArrayList of two syllable words
		array4 = new ArrayList<String>(), //ArrayList of three syllable words
		array5 = new ArrayList<String>(), //ArrayList of four syllable words
		array6 = new ArrayList<String>(), //ArrayList of five syllable words
		array7 = new ArrayList<String>(), //ArrayList of six syllable words
		array8 = new ArrayList<String>(); //ArrayList of seven syllable words
		
		while(dictScanner.hasNext()) {
			
			/* Grabs the next line in the dictionary and assigns it to String "line".
			 */
			
			String line = dictScanner.nextLine();
			String delims = ("[ ]+");
			
			/* Creates an array of strings whose elements are the tokens
			 * that have been split up from the String "line". 
			 */
			
			String[] tokens = line.split(delims);
			
			int counter = 0; //Counts how many vowels each dictionary entry contains
			
			/* This for loop iterates through each token and counts the 
			 * number of tokens with a vowel in them. Because of the way
			 * the CM Dictionary is set up, doing this provides a syllable 
			 * count of each word with minimal margin for error.
			 */
			
			for (String token : tokens) {
				
				if (token.contains("A") || 
					token.contains("E") || 
					token.contains("I") ||
					token.contains("O") || 
					token.contains("U") || 
					token.contains("Y")) {
					counter++;
				}
			}
			
			/* This "if" statement filters out duplicate entries, which 
			 * are recognizable because they contain numbers.
			 */
			
			if (tokens[0].contains("0") == false &&
				tokens[0].contains("1") == false &&
				tokens[0].contains("2") == false &&
				tokens[0].contains("3") == false &&
				tokens[0].contains("4") == false &&
				tokens[0].contains("5") == false &&
				tokens[0].contains("6") == false &&
				tokens[0].contains("7") == false &&
				tokens[0].contains("8") == false &&
				tokens[0].contains("9") == false ) {
				
				/* The following switch statement routes the first element
				 * of the array tokens[] (the word itself, which is tokens[0]
				 * into the appropriate array depending on how many vowels it has.
				 */
				
				switch (counter) {
				
				case 2: array2.add(tokens[0]);
					break;
				
				case 3: array3.add(tokens[0]);
					break;
				
				case 4: array4.add(tokens[0]);
					break;
				
				case 5: array5.add(tokens[0]);
					break;
				
				case 6: array6.add(tokens[0]);
					break;
				
				case 7: array7.add(tokens[0]);
					break;
					
				case 8: array8.add(tokens[0]);
					break;
	
				default: break;
					
				}
			}
		}
		
		/* The following integers will randomly select how many syllables each word of 
		 * each line of the haiku will contain. For example, "random1" chooses a random 
		 * number between 1 and 5, for a word between 1 and 5 syllables. The switch 
		 * statements below ensure the haiku has the right number of syllables per line
		 * after the number of syllables in the first word has been randomized.
		 */
		
		int random1 = (int) (Math.random()*5 + 1);
		int random2 = (int) (Math.random()*7 + 1);
		int random3 = (int) (Math.random()*5 + 1);
		
		/* Each of the switch statements below randomly selects words with the correct
		 * number of syllables needed to create a 5-7-5 haiku.
		 */
		
		switch(random1) {
		
		case 1: System.out.println(array2.get((int) (Math.random()*array2.size())) + " "
				+ array5.get((int) (Math.random()*array5.size())));
			break;
			
		case 2: System.out.println(array3.get((int) (Math.random()*array3.size())) + " " 
				+ array4.get((int) (Math.random()*array4.size())));
			break;
			
		case 3: System.out.println(array4.get((int) (Math.random()*array4.size())) + " " 
				+ array3.get((int) (Math.random()*array3.size())));
			break;
			
		case 4: System.out.println(array5.get((int) (Math.random()*array5.size())) + " " 
				+ array2.get((int) (Math.random()*array2.size())));
			break;
			
		case 5: System.out.println(array6.get((int) (Math.random()*array6.size())));
			break;
			
		default: System.out.println(array6.get((int) (Math.random()*array6.size())));
			break;
		}
		
		switch(random2) {
			
		case 1: System.out.println(array2.get((int) (Math.random()*array2.size())) + " "
				+ array7.get((int) (Math.random()*array7.size())));
			break;
			
		case 2: System.out.println(array3.get((int) (Math.random()*array3.size())) + " " 
				+ array6.get((int) (Math.random()*array6.size())));
			break;
			
		case 3: System.out.println(array4.get((int) (Math.random()*array4.size())) + " " 
				+ array5.get((int) (Math.random()*array5.size())));
			break;
			
		case 4: System.out.println(array5.get((int) (Math.random()*array5.size())) + " " 
				+ array4.get((int) (Math.random()*array4.size())));
			break;
			
		case 5: System.out.println(array6.get((int) (Math.random()*array6.size())) + " "
				+ array3.get((int) (Math.random()*array3.size())));
			break;
			
		case 6: System.out.println(array7.get((int) (Math.random()*array7.size())) + " " 
				+ array2.get((int) (Math.random()*array2.size())));
			break;
		
		case 7: System.out.println(array8.get((int) (Math.random()*array8.size())));
			break;
			
		default: System.out.println(array8.get((int) (Math.random()*array8.size())));
			break;
			
		}
		
		switch(random3) {
		
		case 1: System.out.println(array2.get((int) (Math.random()*array2.size())) + " "
				+ array5.get((int) (Math.random()*array5.size())));
			break;
			
		case 2: System.out.println(array3.get((int) (Math.random()*array3.size())) + " " 
				+ array4.get((int) (Math.random()*array4.size())));
			break;
			
		case 3: System.out.println(array4.get((int) (Math.random()*array4.size())) + " " 
				+ array3.get((int) (Math.random()*array3.size())));
			break;
			
		case 4: System.out.println(array5.get((int) (Math.random()*array5.size())) + " " 
				+ array2.get((int) (Math.random()*array2.size())));
			break;
			
		case 5: System.out.println(array6.get((int) (Math.random()*array6.size())));
			break;
			
		default: System.out.println(array6.get((int) (Math.random()*array6.size())));
			break;
		}
	}
}
